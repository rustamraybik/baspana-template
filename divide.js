function nextStep(number, total) {
  event.preventDefault();

  if (number < total) {
    const currentStepForm = document.getElementById(`step${number}`);
    const prevStepForm = document.getElementById(`step${number - 1}`);
    const currentStepDiv = document.getElementById(`deposit${number}`);
    const prevStepDiv = document.getElementById(`deposit${number - 1}`);

    currentStepForm.classList.add("active");
    prevStepForm.classList.add("finished");
    prevStepForm.classList.remove("active");
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  } else if (number === total) {
    const prevStepForm = document.getElementById(`step${number - 1}`);
    const currentStepDiv = document.getElementById(`deposit${number}`);
    const prevStepDiv = document.getElementById(`deposit${number - 1}`);

    prevStepForm.classList.add("finished");
    prevStepForm.classList.remove("active");
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  }
  if (number === 4) {
    countDown();
  }
}

function onSubmitStep3(number) {
  event.preventDefault();
  nextStep(number);
}
let timeMinut = 119;

function countDown() {
  timer = setInterval(function () {
    let timerShow = document.getElementById("timer");
    let timerText = document.getElementById("timerText");

    let seconds = timeMinut % 60;
    let minutes = (timeMinut / 60) % 60;

    if (timeMinut <= 0) {
      clearInterval(timer);
      timerText.classList.add("d-none");

      timerButton.removeAttribute("disabled");
    } else {
      let strTimer = `${Math.trunc(minutes)}:${seconds}`;

      timerShow.innerHTML = strTimer;
    }
    --timeMinut;
  }, 1000);
}
function sendCode() {
  timerButton.setAttribute("disabled", "true");
  timeMinut = 119;
  let timerShow = document.getElementById("timer");
  let timerText = document.getElementById("timerText");
  timerText.classList.remove("d-none");
  let seconds = timeMinut % 60;
  let minutes = (timeMinut / 60) % 60;
  let strTimer = `${Math.trunc(minutes)}:${seconds}`;

  timerShow.innerHTML = strTimer;
  countDown();
}

function showTooltip() {
  const tooltip = document.getElementById("tooltip");
  const backdrop = document.getElementById("backdrop");
  tooltip.classList.add("c-tooltip_content_active");
  backdrop.classList.remove("d-none");
}

function closeTooltip() {
  const tooltip = document.getElementById("tooltip");
  const backdrop = document.getElementById("backdrop");
  tooltip.classList.remove("c-tooltip_content_active");
  backdrop.classList.add("d-none");
}
