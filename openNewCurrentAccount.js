function nextStep(number, total) {
  event.preventDefault();
  if (number < total) {
    const currentStepDiv = document.getElementById(`account${number}`);

    currentStepDiv.classList.remove("d-none");
    for (let i = 1; i < number; i++) {
      const prevStepDiv = document.getElementById(`account${i}`);

      prevStepDiv.classList.add("d-none");
    }
  } else if (number === total) {
    const prevStepDiv = document.getElementById(`account${number - 1}`);
    const currentStepDiv = document.getElementById(`account${number}`);
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  }
  if (number === 3) {
    countDown();
  }
}

let timeMinut = 119;

function countDown() {
  timer = setInterval(function () {
    let timerShow = document.getElementById("timer");
    let timerText = document.getElementById("timerText");

    let seconds = timeMinut % 60;
    let minutes = (timeMinut / 60) % 60;

    if (timeMinut <= 0) {
      clearInterval(timer);
      timerText.classList.add("d-none");

      timerButton.removeAttribute("disabled");
    } else {
      let strTimer = `${Math.trunc(minutes)}:${seconds}`;

      timerShow.innerHTML = strTimer;
    }
    --timeMinut;
  }, 1000);
}
function sendCode() {
  timerButton.setAttribute("disabled", "true");
  timeMinut = 119;
  let timerShow = document.getElementById("timer");
  let timerText = document.getElementById("timerText");
  timerText.classList.remove("d-none");
  let seconds = timeMinut % 60;
  let minutes = (timeMinut / 60) % 60;
  let strTimer = `${Math.trunc(minutes)}:${seconds}`;

  timerShow.innerHTML = strTimer;
  countDown();
}

function toggleDisableCheck() {
  const cancelButton = document.getElementById("fatca-button");
  cancelButton.toggleAttribute("disabled");
}
