$(document).ready(function () {
  $('input[name="daterange"]').daterangepicker({
    locale: {
      format: "YYYY-MM-DD",
    },
    startDate: moment().subtract(1, "month"),
    endDate: moment(),
  });
});
function setShowActions() {
  const trigger = document.getElementById("actions-trigger");
  trigger.classList.toggle("deposit_item__actions-active");
  const actions = document.getElementById("actions");
  actions.classList.toggle("d-none");
}

function setShowDetails() {
  const show = document.getElementById("showDetails");
  const hide = document.getElementById("hideDetails");
  const details = document.getElementById("details");
  show.classList.toggle("d-none");
  hide.classList.toggle("d-none");
  details.classList.toggle("credit_item__details-card-content-active");
}
