function nextStep(number, total) {
  event.preventDefault();
  if (number === 1) {
    const progressBar = document.getElementById(`progressbar`)
    const currentStepForm = document.getElementById(`step${number}`);
    const currentStepDiv = document.getElementById(`deposit${number}`);
    const nextStepDiv = document.getElementById(`deposit${number + 1}`);
    progressBar.classList.remove('d-none')
    currentStepForm.classList.add('active')
    currentStepDiv.classList.add('d-none')
    nextStepDiv.classList.remove('d-none')
  } else if (number < total) {
    const currentStepForm = document.getElementById(`step${number -1}`);
    const prevStepForm = document.getElementById(`step${number - 2}`);
    const currentStepDiv = document.getElementById(`deposit${number}`);
    const prevStepDiv = document.getElementById(`deposit${number - 1}`);

    currentStepForm.classList.add("active");
    prevStepForm.classList.add("finished");
    prevStepForm.classList.remove("active");
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  } else if (number === total) {
    const prevStepForm = document.getElementById(`step${number - 2}`);
    const currentStepDiv = document.getElementById(`deposit${number}`);
    const prevStepDiv = document.getElementById(`deposit${number - 1}`);

    prevStepForm.classList.add("finished");
    prevStepForm.classList.remove("active");
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  }
  if (number === 4) {
    countDown();
  }
}

function toggleDisableCheck() {
  const cancelButton = document.getElementById("fatca-button");
  cancelButton.toggleAttribute("disabled");
}