$(document).ready(function () {
  $('input[name="daterange"]').daterangepicker({
    locale: {
      format: "YYYY-MM-DD",
    },
    startDate: moment().subtract(1, "month"),
    endDate: moment(),
  });
});