function copyAlterCode() {
  const copyText = document.getElementById("alterCode");

  const $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(copyText).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
