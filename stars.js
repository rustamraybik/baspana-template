$('.rating').starRating({
    starIconEmpty: 'fa fa-star-o',
    starIconFull: 'fa fa-star-o',
    starColorEmpty: 'lightgray',
    starColorFull: '#F05E22',
    starsSize: 28, // em
    stars: 5,
  });
  