function nextStep(number, total) {
  event.preventDefault();

  if (number < total) {
    const currentStepForm = document.getElementById(`step${number}`);
    const currentStepDiv = document.getElementById(`stepDiv${number}`);

    currentStepForm.classList.add("active");
    currentStepDiv.classList.remove("d-none");
    for (let i = 1; i < number; i++) {
      const prevStepDiv = document.getElementById(`stepDiv${i}`);
      const prevStepForm = document.getElementById(`step${i}`);
      prevStepDiv.classList.add("d-none");
      prevStepForm.classList.add("finished");
      prevStepForm.classList.remove("active");
    }
  } else if (number === total) {
    const prevStepForm = document.getElementById(`step${number - 1}`);
    const currentStepDiv = document.getElementById(`stepDiv${number}`);
    const prevStepDiv = document.getElementById(`stepDiv${number - 1}`);

    prevStepForm.classList.add("finished");
    prevStepForm.classList.remove("active");
    currentStepDiv.classList.remove("d-none");
    prevStepDiv.classList.add("d-none");
  }
  if (number === 4) {
    countDown();
  }
}

function signStep3() {
  const documentDiv = document.getElementById("stepDiv3-document");
  const messageDiv = document.getElementById("stepDiv3-message");
  documentDiv.classList.add("d-none");
  messageDiv.classList.remove("d-none");
}

let timeMinut = 119;

function countDown() {
  timer = setInterval(function () {
    let timerShow = document.getElementById("timer");
    let timerText = document.getElementById("timerText");

    let seconds = timeMinut % 60;
    let minutes = (timeMinut / 60) % 60;

    if (timeMinut <= 0) {
      clearInterval(timer);
      timerText.classList.add("d-none");

      timerButton.removeAttribute("disabled");
    } else {
      let strTimer = `${Math.trunc(minutes)}:${seconds}`;

      timerShow.innerHTML = strTimer;
    }
    --timeMinut;
  }, 1000);
}
function sendCode() {
  timerButton.setAttribute("disabled", "true");
  timeMinut = 119;
  let timerShow = document.getElementById("timer");
  let timerText = document.getElementById("timerText");
  timerText.classList.remove("d-none");
  let seconds = timeMinut % 60;
  let minutes = (timeMinut / 60) % 60;
  let strTimer = `${Math.trunc(minutes)}:${seconds}`;

  timerShow.innerHTML = strTimer;
  countDown();
}
