var picker = new Lightpick({
    field: document.getElementById('daterangepicker_1'),
    secondField: document.getElementById('daterangepicker_2'),
    singleDate: false,
    onSelect: function(start, end){
        var str = '';
        str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
        str += end ? end.format('Do MMMM YYYY') : '...';
        // document.getElementById('result-3').innerHTML = str;
    }
});