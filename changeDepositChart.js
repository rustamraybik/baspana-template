new CircleProgress(".progress", {
  max: 100,
  value: 75,
  textFormat: "percent",
  startAngle: -90,
});
